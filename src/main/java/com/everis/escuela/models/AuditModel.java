package com.everis.escuela.models;
import java.io.Serializable;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
    value = {"createAt", "updateAt"},
            allowGetters = true
)
public abstract class AuditModel implements Serializable{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_at")
    private Calendar createAt;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_at")
    private Calendar updateAt;
    
    @PrePersist
    protected void onCreate() {
        createAt = Calendar.getInstance();
    }
    
    @PreUpdate
    protected void onUpdate() {
        updateAt = Calendar.getInstance();
    }
}