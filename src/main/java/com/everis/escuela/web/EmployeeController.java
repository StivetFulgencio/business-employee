package com.everis.escuela.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.escuela.models.Employee;
import com.everis.escuela.service.EmployeeService;

@RestController
@RequestMapping("/api/business/v1")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeServ = null;

//	@RequestMapping("/employees/{id}")
//	public String getEmployeeById(@PathVariable(value = "id") Long employeeId){
//		
//		return "ID Empleado: " + String.valueOf(employeeId);
//	}
	
	@PostMapping("/employees")
	public String getEmployeeById(@RequestBody Employee employee){
		this.employeeServ.saveEmployee(employee);
		return "Se ha guardado exitosamente.";
	}
}
